# workflow-metadata-loader

The workflow-metadata-loader runs every 5 mins (adjustable in the yaml). It collects all "metadata" documents in precmed database in MongoDB that have their flag "finished" equal to 0. 
The fields of metadata are the following. 

        workflowType: One of the workflow types that rest api provides.

        userId: The user id that submitted this workflow.

        username: The usernmae of that user. 

        group: The group the user belongs to.

        orderId: The order Id the user gave when submitting.

        end: When the workflow finished if it did.

        start: When Cromwell started the workflow.

        status: The status of the workflow.

        submission_date_time: When the submission happened.

        finished: Flag used by the load to know if it will process or not this entry.
          
        failures_dict: A json with the failures messages as they are returned by Cromwell. Internally it is stored as a dictionary.
  
        outputs_dict: A json with the outputs location as they are returned by Cromwell. Internally it is stored as a dictionary.


Specifically failures_dict is a json of the following form

        {failures: [FAILURES]} where FAILURES has the following recursive schema 
        
        FAILURES = {message: (string), causedBy: [FAILURES]}

Then for each one of them with finished = 0, it asks the Cromwell api to retrieve metadata for this workflow id and updates the document accordingly. 

If the status has changed to "Succeeded" or "Failed" it sets finished to 1, marking the document so the loader will skip it in future runs.
